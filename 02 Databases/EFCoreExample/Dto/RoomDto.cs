﻿using EFCoreExample.DataAccess.Entity;

namespace EFCoreExample.Dto
{
    public class RoomDto
    {
        public string Name { get; set; }
        public decimal Price { get; set; }

        public Room ToRoom()
        {
            return new Room
            {
                Name = Name,
                Price = Price

            };
        }
        public static RoomDto FromRoom(Room room)

        {
            return new RoomDto
            {
                Name = room.Name,
                Price = room.Price

            };

        }
    }
}

