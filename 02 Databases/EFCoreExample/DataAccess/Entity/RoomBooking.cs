﻿namespace EFCoreExample.DataAccess.Entity
{
    public class RoomBooking
    {
        public int BookingId { get; set; }
        public Booking Booking { get; set; }
        public int RoomId { get; set; }
        public Room Room { get; set; }

    }
}
