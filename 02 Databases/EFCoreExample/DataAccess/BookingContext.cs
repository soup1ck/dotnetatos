﻿using EFCoreExample.DataAccess.Entity;
using Microsoft.EntityFrameworkCore;

namespace EFCoreExample.DataAccess
{
	public class BookingContext : DbContext
	{
		public BookingContext(DbContextOptions options) : base(options) { }

		public DbSet<User> Users { get; set; }
		public DbSet<Booking> Bookings { get; set; }
		public DbSet<Room> Rooms { get; set; }
		public DbSet<RoomBooking> RoomBookings { get; set;}
		
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Booking>()
				.HasOne(x => x.User)
				.WithMany(u => u.Bookings)
				.HasForeignKey(b => b.UserId)
				.IsRequired(true);

			modelBuilder.Entity<Booking>()
				.Property(b => b.FromUtc)
				.IsRequired(true);

			modelBuilder.Entity<Booking>()
				.Property(b => b.ToUtc)
				.IsRequired(true);

			modelBuilder.Entity<RoomBooking>()
				.HasKey(rb => new {rb.BookingId,rb.RoomId });

			modelBuilder.Entity<RoomBooking>()
				.HasOne(rb=>rb.Booking)
				.WithMany(b => b.RoomBookings)
				.HasForeignKey(rb=>rb.BookingId)
				.IsRequired(true);

			modelBuilder.Entity<RoomBooking>()
				.HasOne(rb=>rb.Room)
				.WithMany(r=> r.RoomBookings)
				.HasForeignKey(rb=>rb.RoomId)
				.IsRequired(true);

			/*
			SELECT * FROM public."Bookings"
			WHERE "FromUtc" > '2022-08-02 20:51:37+00'
			vs
			SELECT * FROM public."Bookings"
			WHERE "Comment" like '%Пере%'
			 */
			modelBuilder.Entity<Booking>()
				.HasIndex(b => b.FromUtc);
			modelBuilder.Entity<Booking>()
				.HasIndex(b => b.ToUtc);
		}
	}
}
