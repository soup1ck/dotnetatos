﻿using EFCoreExample.DataAccess;
using EFCoreExample.DataAccess.Entity;
using EFCoreExample.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace EFCoreExample.Controllers
{
	[Route("room")]
	public class RoomController : Controller
	{
		private readonly BookingContext _bookingContext;

		public RoomController(BookingContext bookingContext)
		{
			_bookingContext = bookingContext;
		}
		public async Task<IActionResult> GetAllRooms(CancellationToken cancellationToken)
		{
			var rooms = await _bookingContext.Rooms
				.ToArrayAsync(cancellationToken);
			return Ok(rooms.Select(r => RoomDto.FromRoom(r)));
		}
		[HttpPost]
		public async Task<ActionResult<RoomDto>> CreateRoom([FromBody] RoomDto roomDto)
		{
			if (string.IsNullOrEmpty(roomDto.Name))
				return BadRequest("Name cannot be empty");
			if (roomDto.Price == default)
				return BadRequest("Price cannot be empty");

			var room = roomDto.ToRoom();

			var roomInDb = await _bookingContext
				.Rooms
				.FirstOrDefaultAsync(r => r.Name == room.Name);

			if (roomInDb != null)
			{
				return Conflict("Room with this name alredy exists");
			}

			await _bookingContext.Rooms.AddAsync(room);
			return Ok(RoomDto.FromRoom(room));
		}
		public async Task<ActionResult<IEnumerable<RoomDto>>> SearchEmptyRoom(DateTime fromUtc, DateTime toUtc)
		{
			var emptyRooms = await _bookingContext
			.Rooms
			.Where(r => !r.RoomBookings.Any(rb => rb.Booking.ToUtc <= fromUtc || rb.Booking.FromUtc>=toUtc))
			.ToListAsync();
			return Ok(emptyRooms);

		}
	}
}
