﻿using EFCoreExample.DataAccess;
using EFCoreExample.DataAccess.Entity;
using EFCoreExample.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LoggingAndConfiguration.Configuration;
using Microsoft.Extensions.Logging;

namespace EFCoreExample.Controllers
{
	[Route("booking")]
	public class BookingController : Controller
	{
		private readonly BookingContext _bookingContext;
		private readonly IOptions<EntityConfiguration> _entityConfiguration;
		private readonly ILogger<BookingController> _logger;
		

		public BookingController(BookingContext bookingContext, 
			IOptions<EntityConfiguration> entityConfiguration, 
			ILogger<BookingController> logger)
		{
			_bookingContext = bookingContext;
			_entityConfiguration = entityConfiguration;
			_logger = logger;
		}
		
		[Route("{id}")]
		public IActionResult Get(int id)
		{
			return Ok(id);
		}

		public async Task<IActionResult> GetAll(CancellationToken cancellationToken)
		{
			var bookings = await _bookingContext.Bookings
				.Include(b => b.User)
				.ToArrayAsync(cancellationToken);
			return Ok(bookings.Select(b => BookingDto.FromBooking(b)));
		}

		[HttpPost]
		public async Task<ActionResult<BookingDto>> CreateBooking(BookingDto bookingDto)
		{
			if (string.IsNullOrEmpty(bookingDto.Username))
			{
				_logger.LogWarning("Username is empty");
				return BadRequest("Username cannot be empty");
			}
			if (bookingDto.FromUtc == default)
			{
				_logger.LogWarning("FromUtc is empty");
				return BadRequest("FromUtc cannot be empty");
			}
			if (bookingDto.ToUtc == default)
			{
				_logger.LogWarning("ToUtc is empty");
				return BadRequest("ToUtc cannot be empty");
			}

			User user = await _bookingContext
				.Users
				.FirstOrDefaultAsync(u => u.UserName == bookingDto.Username);
			if (user is null)
			{
				_logger.LogWarning("User with this name cannot be found");
				return BadRequest($"User with name '{bookingDto.Username}' cannot be found");
			}


			Booking newBooking = bookingDto
				.ToBooking(userId: user.Id);
			var alreadyCreatedBooking = await _bookingContext
				.Bookings
				.FirstOrDefaultAsync(b =>
					b.FromUtc <= newBooking.FromUtc
					&& b.ToUtc >= newBooking.FromUtc
					|| b.FromUtc <= newBooking.ToUtc
					&& b.ToUtc >= newBooking.ToUtc
					|| b.FromUtc == newBooking.FromUtc
					&& b.ToUtc == newBooking.ToUtc);
			if (alreadyCreatedBooking is not null)
			{
				_logger.LogWarning("This time is busy");
				return Conflict("Booking for this time has already been created");
			}
			if (newBooking.FromUtc < DateTime.UtcNow)
			{
				_logger.LogWarning("Oooooops this time is earlier than now");
                return BadRequest("Cannot have from date earlier than now");
            }
			if (newBooking.ToUtc - newBooking.FromUtc <= TimeSpan.FromMinutes(_entityConfiguration.Value.MinInterval))
			{
				_logger.LogWarning("Time for booking less than 30 min");
                return BadRequest("Booking period should be at lease 30 minutes long");
            }

			_bookingContext.Add(newBooking);
			await _bookingContext.SaveChangesAsync();
			return Ok(BookingDto.FromBooking(newBooking));
		}
	}
}
